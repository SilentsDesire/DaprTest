using Common;
using Dapr.Client;
using Grpc.Core;
using GrpcGreeter;
using Microsoft.AspNetCore.Mvc;
using System.Net.Http;
using System.Security.Principal;
using System.Text.Encodings.Web;
using System.Text.Json;
using System.Text.Unicode;
using System.Threading;
using System.Transactions;
using static Google.Rpc.Context.AttributeContext.Types;

namespace Client.Controllers
{
    [ApiController]
    [Route("[controller]/[action]")]
    public class ClientTestController : ControllerBase
    {
        private readonly ILogger<ClientTestController> _logger;
        private readonly TestHello.TestHelloClient _client;
        private readonly DaprClient _daprClient;

        public ClientTestController(ILogger<ClientTestController> logger, TestHello.TestHelloClient client, DaprClient daprClient)
        {
            _logger = logger;
            _client = client;
            _daprClient = daprClient;
        }

        /// <summary>
        /// get获取
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult Get()
        {
            return Ok("客户端：" + DateTime.Now.ToString());
        }
        /// <summary>
        /// 发送order
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<ActionResult> SendOrder()
        {

            // var client = DaprClient.CreateInvokeHttpClient(appId: "Service");
            //using var DaprClient = new DaprClientBuilder().Build();
            for (int i = 1; i <= 100; i++)
            {
                var order = new Order(i);

                //var cts = new CancellationTokenSource();
                //Console.CancelKeyPress += (object? sender, ConsoleCancelEventArgs e) => cts.Cancel();

                ////// Invoking a service
                //Console.WriteLine("Order SendOrder: " + order);
                //var response = await client.PostAsJsonAsync("/ServiceTest/ReceiveOrder", order, cts.Token);
                //var result = await response.Content.ReadAsStringAsync();
                //Console.WriteLine("Order Result:" + result);

                // 使用daprclient的话，服务返回的数据必须是json对象,不然要报错
                var result = await _daprClient.InvokeMethodAsync<object, Test>("Service", "/ServiceTest/ReceiveOrder", order);
                Console.WriteLine("Order SendOrder: " + order + " response:" + JsonSerializer.Serialize(result));
                await Task.Delay(TimeSpan.FromSeconds(10));

            }
            return Ok("客户端：" + DateTime.Now.ToString());
        }
        /// <summary>
        /// 获取服务端Ip
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<ActionResult> GetServiceIp()
        {
            var result = await _daprClient.InvokeMethodAsync<AppAddress>(HttpMethod.Get, "Service", "ServiceTest/GetIp");
            return Ok(result);
        }
        /// <summary>
        /// 获取Ip
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult GetIp()
        {
            var address = AppAddress.Instance;
            return Ok(address);
        }
        /// <summary>
        /// Grpc测试
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<ActionResult> GrpcAsync()
        {
            var result = await _daprClient.InvokeMethodGrpcAsync<HelloRequest, HelloReply>("Service", "sayhi", new HelloRequest { Name = "aaa" });
            return Ok(result);
        }
        /// <summary>
        /// Grpc测试2
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<ActionResult> Grpc2Async()
        {
            var result = await _client.SayHelloAsync(new HelloRequest { Name = "测试" });
            return Ok(result);
        }
        /// <summary>
        /// 生产消息
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<ActionResult> PublishAsync()
        {
            var data = new Test() { Age = 88, Name = "test_topic" };
            await _daprClient.PublishEventAsync<Test>("pubsub", "test_topic", data);
            Console.WriteLine("Publis Message Success ");
            return Ok("发送消息成功");
        }
        public record Order([property: System.Text.Json.Serialization.JsonPropertyName("orderId")] int OrderId);
        public class Test
        {
            public int Age { get; set; } = 1;
            public string Name { get; set; }
        }
    }
}
