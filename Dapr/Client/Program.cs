using Common;

namespace Client
{
    public class Program
    {
        public static void Main(string[] args)
        {
            var builder = WebApplication.CreateBuilder(args);
            // ���������ļ�
            var configuration = new ConfigurationBuilder().AddJsonFile("appsettings.json").Build();
            var port = configuration.GetValue<int>("Urls:Port");
            AppAddress.SetInstance(port);
            builder.WebHost.UseUrls($"http://*:{port}");
            // Add services to the container.

            builder.Services.AddControllers().AddDapr();
            // Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
            builder.Services.AddEndpointsApiExplorer();
            builder.Services.AddSwaggerGen();
            builder.Services.AddGrpcClient<GrpcGreeter.TestHello.TestHelloClient>(o =>
            {
                o.Address = new Uri("http://localhost:6200");
            });
            //builder.AddCustomHealthChecks();
            var app = builder.Build();

            // Configure the HTTP request pipeline.
            if (app.Environment.IsDevelopment())
            {
                app.UseSwagger();
                app.UseSwaggerUI();
            }

            app.UseAuthorization();


            app.MapControllers();
            //app.MapHealthChecks("/healthz");
            app.Run();
        }

    }

}
