using Dapr;
using Microsoft.AspNetCore.Mvc;
using System.Net.NetworkInformation;
using System.Net;
using System.Text;
using static Service.Controllers.ServiceTestController;
using Common;

namespace Service.Controllers
{
    [ApiController]
    [Route("[controller]/[action]")]
    public class ServiceTestController : ControllerBase
    {
        private readonly ILogger<ServiceTestController> _logger;
        public ServiceTestController(ILogger<ServiceTestController> logger)
        {
            _logger = logger;
        }

        /// <summary>
        /// get获取
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult Get()
        {
            return Ok(new { Result = "服务端：" + DateTime.Now.ToString() });
        }

        /// <summary>
        /// 接收order
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public ActionResult<Test> ReceiveOrder([FromBody] Order order)
        {
            Console.WriteLine("Order received : " + order);
            return new Test { Name = "Success:" + order.orderId, Age = 22 };
            //return "接收成功:" + order.orderId;
        }
        /// <summary>
        /// 获取Ip
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult GetIp()
        {
            var address = AppAddress.Instance;
            return Ok(address);
        }
        /// <summary>
        /// 发布订阅
        /// </summary>
        /// <returns></returns>
        [Topic("pubsub", "test_topic")]
        [HttpPost]
        public async Task<ActionResult> Sub()
        {
            Stream stream = Request.Body;
            byte[] buffer = new byte[Request.ContentLength.Value];
            stream.Position = 0L;
            await stream.ReadAsync(buffer, 0, buffer.Length);
            string content = Encoding.UTF8.GetString(buffer);
            _logger.LogInformation("testsub" + content);
            return Ok(content);
        }


        public record Order(int orderId);
        public class Test
        {
            public int Age { get; set; } = 1;
            public string Name { get; set; }
        }
    }
}
