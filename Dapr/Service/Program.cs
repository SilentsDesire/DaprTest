using Common;
using Google.Api;
using HealthChecks.UI.Client;
using System.Net.Sockets;
using System.Net;

namespace Service
{
    public class Program
    {
        public static void Main(string[] args)
        {
            //// 创建一个临时的 Socket，不连接到任何地址
            //using (Socket socket = new Socket(AddressFamily.InterNetwork, SocketType.Dgram, ProtocolType.IP))
            //{
            //    socket.Connect("8.8.8.8", 65530);  // 使用 Google 的 DNS 服务器
            //    IPEndPoint endPoint = socket.LocalEndPoint as IPEndPoint;

            //    if (endPoint != null)
            //    {
            //        Console.WriteLine($"Local IP address: {endPoint.Address}");
            //    }
            //    else
            //    {
            //        Console.WriteLine("Failed to retrieve local IP address");
            //    }
            //}


            var builder = WebApplication.CreateBuilder(args);
            // 加载配置文件
            var configuration = new ConfigurationBuilder().AddJsonFile("appsettings.json").Build();
            var port = configuration.GetValue<int>("Urls:Port");
            AppAddress.SetInstance(port);
            builder.WebHost.UseUrls($"http://*:{port}");//这里如果使用grpc的话，要设置为https
            // Add services to the container.

            builder.Services.AddControllers().AddDapr();
            // Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
            builder.Services.AddEndpointsApiExplorer();
            builder.Services.AddSwaggerGen();
            builder.Services.AddGrpc();//注入gRPC 
                                       //builder.AddCustomHealthChecks();
                                       // builder.Services.AddHealthChecks();
            var app = builder.Build();
            // Configure the HTTP request pipeline.
            if (app.Environment.IsDevelopment())
            {
                app.UseSwagger();
                app.UseSwaggerUI();
            }
            app.UseAuthorization();
            app.UseCloudEvents();
            app.MapGrpcService<HelloService>();
            app.MapGrpcService<TestHelloService>();
            //app.MapCustomHealthChecks("/hc", "/liveness", UIResponseWriter.WriteHealthCheckUIResponse);
            app.MapControllers();
            app.MapSubscribeHandler();
            //app.MapHealthChecks("/healthz");
            app.Run();
        }


    }
}
